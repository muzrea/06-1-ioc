package com.twuc.webApp.simplest.scanning;

import org.springframework.stereotype.Component;

@Component
public class Dependent {
    private String age = "1";

    public Dependent() {
        this.age = age;
    }

    public String getAge() {
        return age;
    }
}
