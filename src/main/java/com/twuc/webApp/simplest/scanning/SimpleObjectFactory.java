package com.twuc.webApp.simplest.scanning;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleObjectFactory {
    @Bean
    public SimpleObject createSimpleObject(SimpleDependent simpleDependent){
        simpleDependent.setName("O_o");
        return new SimpleObject(simpleDependent);
    }
}
