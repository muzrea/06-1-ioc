package com.twuc.webApp.simplest.scanning;

import org.springframework.stereotype.Component;

@Component
public class WithDependency {
    private Dependent dependent;

    public WithDependency(Dependent dependent) {
        this.dependent = dependent;
    }
}
