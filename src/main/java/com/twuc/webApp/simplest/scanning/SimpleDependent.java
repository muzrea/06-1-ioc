package com.twuc.webApp.simplest.scanning;

import org.springframework.stereotype.Component;

@Component
public class SimpleDependent {
    private String name;

    public SimpleDependent() {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
