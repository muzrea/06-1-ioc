package com.twuc.webApp.simplest.scanning;

public class SimpleObject implements SimpleInterface {
    public SimpleDependent simpleDependent;

    public SimpleObject(SimpleDependent simpleDependent) {
        this.simpleDependent = simpleDependent;
    }
}
