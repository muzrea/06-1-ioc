package com.twuc.webApp.yourTurn;

import com.twuc.webApp.simplest.scanning.*;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class IocTest {
    @Test
    void should_create_object_withoutDependency() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.simplest.scanning");
        WithoutDependency bean = context.getBean(WithoutDependency.class);
        assertNotNull(bean);
    }

    @Test
    void should_create_withoutDependency() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.simplest.scanning");
        WithoutDependency bean = context.getBean(WithoutDependency.class);
        WithoutDependency bean2 = context.getBean(WithoutDependency.class);
        assertNotEquals(bean, bean2);
    }

    @Test
    void should_create_object_withDependency_dependent() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.simplest.scanning");
        WithDependency bean = context.getBean(WithDependency.class);
        assertNotNull(bean);
    }

    @Test
    void should_create_object_failed_out_of_scanning() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.simplest.scanning");
        assertThrows(RuntimeException.class, () -> context.getBean(OutOfScanningScope.class));
    }

    @Test
    void should_create_object_use_interface_class() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.simplest.scanning");
        InterfaceImpl bean = (InterfaceImpl) context.getBean(Interface.class);
        assertNotNull(bean);
    }

    @Test
    void should_create_object_when_use_class_implements_interface() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.simplest.scanning");
        InterfaceImpl bean = (InterfaceImpl) context.getBean(Interface.class);
        assertNotNull(bean);
    }

    @Test
    void should_create_object_when_have_dependency() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.simplest.scanning");
        SimpleObject bean = (SimpleObject) context.getBean(SimpleInterface.class);
        SimpleDependent simpleDependent = new SimpleDependent();
        assertEquals(bean.simpleDependent.getName(), "O_o");
    }

    @Test
    void should_create_object_when_have_dependency_use_autowired() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.simplest.scanning");
        MultipleConstructor bean = context.getBean(MultipleConstructor.class);
        assertEquals(bean.getDependent().getAge(), "1");
    }
}
